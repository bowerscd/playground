<?php

require_once "../deps/ignite-client/vendor/autoload.php";

use Apache\Ignite\Client;
use Apache\Ignite\ClientConfiguration;
use Apache\Ignite\Cache\CacheConfiguration;
use Apache\Ignite\Cache\CacheEntry;
use Apache\Ignite\Exception\ClientException;
use Apache\Ignite\Query\SqlFieldsQuery;

$client = new Client();

try
{
    $client->connect(new ClientConfiguration("ignite:10800"));

    $cacheNames = $client->cacheNames();
    $cache = $client->getCache("SQL_PUBLIC_EXAMPLETABLE", 
        (new CacheConfiguration())->setSqlSchema('PUBLIC'));


    // iterate over elements returned by the query
    print_r($cacheNames);
    echo("<br>");

    echo($cache->getSize());
    $sqlFieldsCursor = $cache->query(
        (new SqlFieldsQuery("SELECT * FROM ExampleTable"))->
            setPageSize(1));

    // iterate over elements returned by the query
    foreach ($sqlFieldsCursor as $fields) {
        print_r($fields);
    }
}
catch(Exception $e)
{
    echo($e);
}
catch(ClientException $e)
{
    echo($e->getMessage());
}
finally
{
    $client->disconnect();
}
echo("</table>");

print "Hello, World!";

?>