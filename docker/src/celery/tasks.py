from celery import Celery
from os import environ

user = environ["RABBITMQ_DEFAULT_USER"]
password = environ["RABBITMQ_DEFAULT_PASS"]
vhost = environ["RABBITMQ_DEFAULT_VHOST"]

BrokerCnxString = f'amqp://{user}:{password}@rabbitmq:5672/{vhost}'
app = Celery('tasks', backend="rpc://", broker=BrokerCnxString)

@app.task
def Echo(arg : str) -> None:
    from datetime import datetime
    x = datetime.now()
    print(f"[{x}] Echo {arg}")

@app.task
def EchoLong(arg: str) -> None:
    from datetime import datetime
    x = datetime.now()
    print(f"[{x}] EchoLong: {arg}")

@app.task(bind=True)
def AddToDatabase(self) -> bool:
    from celery import states, exceptions
    from traceback import format_exc
    from random import randint
    try:
        from pyignite import Client
        igniteClient = Client()
        igniteClient.connect("ignite", 10800)

        igniteClient.sql(
            """
                CREATE TABLE IF NOT EXISTS ExampleTable(
                    K INT PRIMARY KEY,
                    V VARCHAR(256))
            """)

        key = randint(0, 0xFF)
        value = randint(0, 0xFF)

        igniteClient.sql(
            "INSERT INTO ExampleTable(K, V) VALUES (?, ?)",
            query_args=(key, value))

        print(f"put {key}:{value} into cache")

        rows = list(igniteClient.sql("""
            SELECT * FROM ExampleTable ORDER BY K
        """))
        print("\n\n-------------------------\n")
        for row in rows:
            print(f"    {row[0]}    :     {row[1]}")
        print(   f"       Total: {len(rows)}")
        print("\n\n-------------------------\n")

    except Exception as ex:
        self.update_state(
            state = states.FAILURE,
            meta={
                'exc_type': type(ex).__name__,
                'exc_message': format_exc().split('\n')
            })
        raise exceptions.TaskError()

@app.on_after_configure.connect
def DistributedTaskSetup(sender, **kwargs) -> None:
    from celery.schedules import crontab

    # 10s Task
    sender.add_periodic_task(
        10,
        AddToDatabase.s(),
    )

    # # 1 Minute Task
    # sender.add_periodic_task(
    #     60 * 1,
    #     EchoLong.s(str(10)),
    # )
