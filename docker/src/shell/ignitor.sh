#!/bin/bash

/opt/ignite/apache-ignite/bin/control.sh --host ignite --activate

while [ $? -ne 0 ]; do
    sleep 1
    /opt/ignite/apache-ignite/bin/control.sh --host ignite --activate
done

exit 0
