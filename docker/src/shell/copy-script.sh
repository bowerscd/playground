#!/bin/bash
previousDate=""
while true; do
{
    #Interval: 5 minutes
    sleep $1
    currDate=$(date "+%Y.%m.%d::%H:%M:%S")
    echo "backing up @ $currDate"
    tar -cf /mnt/hostmirror/backup-$currDate.tar.gz /mnt/ignite

    # this can be RSYNC'D for a remote deployment here --

    # remove old backup
    rm /mnt/hostmirror/backup-$previousDate.tar.gz
    previousDate=$currDate
} done;