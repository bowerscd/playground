#!/bin/bash

docker build . -f ./docker/ignite-base/Dockerfile -t compose/ignitebase:2.7.5
docker-compose up --build